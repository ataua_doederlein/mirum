import React from 'react'
import styled from 'styled-components'

const Interest = ({ children, onClick }) => {
    return (
        <Div>{children}<Span onClick={onClick}>X</Span></Div>
    )
}

const Div = styled.div`
    display: inline-block;
    background: #d8d8d8;
    padding: 8px;
    margin: 4px;
`

const Span = styled.span`
    font-size: 0.8rem;
    color: red;
    cursor: pointer;
`

export default Interest