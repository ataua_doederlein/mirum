import React from 'react'
import './style.css'

const Button = ({ onClick, children, type, form, value }) => {
    return (
        <button type={type} form={form} value={value} onClick={onClick}>{children}</button>
    )
}

export default Button