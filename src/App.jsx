import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { Home, Aside, Form, Confirm } from './pages'
import styled from 'styled-components'

function App() {

  return (
    <Switch>

      <Route exact path='/'>
        <Home />
      </Route>

      <Route path='/form'>
        <Container>
          <Aside />
          <Form />
        </Container>
      </Route>

      <Route path='/confirm'>
        <Container>
          <Aside />
          <Confirm />
        </Container>
      </Route>
    </Switch>
  );
}

const Container = styled.div`
 display: grid;
 grid-template-columns: 220px 1fr;
`

export default App;
