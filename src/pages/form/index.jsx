import React, { useState } from 'react'
import Button from '../../components/button'
import Interest from '../../components/interest'
import { useDispatch } from 'react-redux'
import { updateData } from '../../redux/actions'
import { useForm } from 'react-hook-form'
import { useHistory } from 'react-router-dom'
import { yupResolver } from '@hookform/resolvers/yup'
import './style.css'
import { contactSchema, estados, Err } from './helper'

const Form = () => {
    const history = useHistory()
    const dispatch = useDispatch()

    const [address, setAddress] = useState(null)
    const [interests, setInterests] = useState([])

    const { register, handleSubmit, errors, watch } = useForm({ resolver: yupResolver(contactSchema) })

    const stripInterest = (interest) => {
        setInterests(interests.filter(x => x !== interest))
    }

    const handleInterests = (data) => {

        let newInterest = data.target.value
        if (newInterest !== '' && newInterest !== ' ' && (newInterest[newInterest.length - 1] === ',' || newInterest[newInterest.length - 1] === ' ')) {
            newInterest = newInterest.replace(',', '').trim()
            if (newInterest !== ' ') {
                setInterests([...interests, newInterest])
            }
            data.target.value = ''
        }
    }

    const sendForm = () => {
        const newdata = { ...watch(), interests: [interests] }
        console.log(newdata)
        dispatch(updateData(newdata))
        history.push('/confirm')
    }

    return (
        <main>
            <form name='newUser' onSubmit={handleSubmit(sendForm)}>

                <label>
                    <span>Nome</span>
                    <div>
                        <input
                            type='text'
                            name='first'
                            placeholder='Primeiro nome'
                            ref={register} />

                        <input type='text'
                            name='last'
                            placeholder='Sobrenome'
                            ref={register} />
                    </div>
                </label>
                <div>
                    <Err>{errors.first?.message}</Err>
                    <Err>{errors.last?.message}</Err>
                </div>

                <label>
                    <span>Idade</span>
                    <div className='age'>
                        <input
                            type='range' min={0} max={3}
                            name='age'
                            defaultValue='0'
                            ref={register} />
                        <div>
                            <span>13-19</span>
                            <span>20-29</span>
                            <span>30-45</span>
                            <span>45 e acima</span>
                        </div>
                    </div>
                </label>

                <label>
                    <span>Email</span>
                    <input
                        type='email'
                        name='email'
                        placeholder='david@example.com'
                        ref={register} />
                </label>
                <Err>{errors.email?.message}</Err>

                <label>
                    <span>Telefone</span>

                    <input
                        type='phone'
                        name='phone'
                        placeholder='(12) 98765-4321'
                        ref={register}
                    />
                </label>
                <Err>{errors.phone?.message}</Err>

                <label>
                    <span>Estado</span>
                    <select
                        name='state'
                        defaultValue='Paraná'
                        ref={register}>
                        {estados.map((estado, key) => <option key={key} value={estado} >{estado}</option>)}
                    </select>
                </label>

                <label>
                    <span>País</span>
                    <select
                        name='country'
                        defaultValue='Brasil'
                        ref={register} >
                        <option value='Argentina'>Ougadogu</option>
                        <option value='Brasil'>Brasil</option>
                        <option value='Uruguai'>XPTO</option>
                    </select>
                </label>

                <label>
                    <span>Endereço</span>
                    <div style={{ display: 'inlineBlock' }}>
                        <select
                            placeholder='Selecione'
                            name='address'
                            defaultValue='choose'
                            onChange={(e) => setAddress(e.target.value)} >
                            <option value='choose' disabled={true}>Escolha:</option>
                            <option value='home'>Residencial</option>
                            <option value='work'>Comercial</option>
                        </select>
                        <input
                            type='text'
                            name='home'
                            placeholder='End. residencial'
                            ref={register}
                            style={{ display: address === 'home' ? 'flex' : 'none' }}
                        />
                        <input
                            type='text'
                            name='work'
                            placeholder='End. comercial'
                            ref={register}
                            style={{ display: address === 'work' ? 'flex' : 'none' }}
                        />
                    </div>
                </label>
                <Err>{errors.home?.message}</Err>
                <Err>{errors.work?.message}</Err>

                <label>
                    <span>Interesses</span>
                    <input
                        className='interests'
                        type='text'
                        placeholder='Futebol, Leitura, Volei'
                        onChange={handleInterests} />
                    <div className='interestsList'>
                        {interests?.map((interest, key) => <Interest
                            name='interests'
                            key={key}
                            onClick={() => stripInterest(interest)}>{interest}</Interest>)}
                    </div>
                </label>

                <label>
                    <span>Novidades</span>
                    <input type='checkbox' name='newsLetter' ref={register} /> &nbsp;Desejo receber novidades por e-mail.
                </label>

            </form>
            <div className='buttonDiv'>
                <Button onClick={handleSubmit(sendForm)}>Salvar</Button>
            </div>
        </main>
    )
}

export default Form