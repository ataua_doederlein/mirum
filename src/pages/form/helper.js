import styled from 'styled-components'
import * as yup from 'yup'

export const contactSchema = yup.object().shape({
    first: yup.string().matches(/^[A-Za-zà-ź]+$/i, 'Nome: Apenas letras são aceitas').max(20, 'Nome: Máximo de 20 caracteres').required('Nome: campo obrigatório'),
    last: yup.string().matches(/^[A-Za-z]+$/i, 'Sobrenome: Apenas letras são aceitas').required('Sobrenome: campo obrigatório'),
    age: yup.number(),
    email: yup.string().email('Insira um endereço de e-mail válido').required('E-mail: Campo obrigatório'),
    phone: yup.string().matches(/\(([0-9]){2}\)\s{0,1}([0-9]){5}-([0-9]){4}/, 'Formato: (12) 98765-4321').required('Fone: Campo obrigatório'),
    state: yup.string().required('Campo obrigatório'),
    country: yup.string().required('Campo obrigatório'),
    home: yup.string().required('End. Residencial: campo obrigatório'),
    work: yup.string(),
    newsLetter: yup.boolean()
});

export const estados = [
    'Acre',
    'Alagoas',
    'Amazonas',
    'Amapá',
    'Bahia',
    'Ceará',
    'Distrito Federal',
    'Espírito Santo',
    'Goiás',
    'Maranhão',
    'Minas Gerais',
    'Mato Grosso do Sul',
    'Mato Grosso',
    'Pará',
    'Paraíba',
    'Pernambuco',
    'Piauí',
    'Paraná',
    'Rio de Janeiro',
    'Rio Grande do Norte',
    'Rondônia',
    'Roraima',
    'Rio Grande do Sul',
    'Santa Catarina',
    'Sergipe',
    'São Paulo',
    'Tocantins',
    'Outro']

export const Err = styled.p`
    color: red;
    font-style: italic;
    text-align: center;
`