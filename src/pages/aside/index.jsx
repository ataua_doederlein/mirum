import React from 'react'

import { useDispatch } from 'react-redux'
import { updateImage } from '../../redux/actions'
import { useForm } from 'react-hook-form'

import styled from 'styled-components';

const Aside = () => {

    const { register, handleSubmit, watch } = useForm()
    const dispatch = useDispatch()

    const onSubmit = () => {
        if (watch().hasOwnProperty('file') && watch().file !== undefined) {
            dispatch(updateImage(watch().file[0]))
        }
    }

    return (
        <StyledAside>
            <img alt='fakeImg' src='https://picsum.photos/180/180' />
            <form>
                <label>
                    <input
                        onChange={handleSubmit(onSubmit)}
                        style={{ display: "none" }}
                        name='file'
                        type='file'
                        ref={register({ required: true })}
                    />
                    <span>Alterar foto</span>
                </label>
            </form>

        </StyledAside>
    )
}

const StyledAside = styled.aside`
    border-right: 1px solid gray;
    text-align: center; 
    padding: 16px;
`

export default Aside