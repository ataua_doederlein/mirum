import React from 'react'
import axios from 'axios'
import './style.css'
import { useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'
import Button from '../../components/button'

const Confirm = () => {

    const userState = useSelector(state => state.data)
    const { first, last, age, email, phone, state, interests, newsletter } = userState
    const history = useHistory()

    const handleConfirm = () => {
        const headers = { "Content-Type": "application/json; charset=utf-8" }
        const url = 'https://my-json-server.typicode.com/ataua/fakeserver-mirum/data'
        axios.post(url, userState, headers).then(res => {
            localStorage.setItem('message', `cadastrado usuário ${res.data.first} com o ID ${res.data.id}`)
            history.push('/')
        }).catch(errors => console.log(errors))
    }
    const idade = () => {
        switch (age) {
            case 0:
                return 'entre 13 e 19'
            case 1:
                return 'entre 20 e 29'
            case 2:
                return 'entre 30 e 45'
            default:
                return 'mais de 45'
        }
    }

    return (
        <main>
            <p>
                Eu sou o {first} {last}, tenho {idade()} anos e você pode enviar-me e-mails para {email}. Eu moro no estado do {state}. {interests && `Eu gosto de ${interests.join(', ')}. `}{newsletter && 'Por favor me envie newsletters. '}Para me contactar, ligue no fone {phone}.
            </p>
            <div className='buttonDiv'>
                <Button onClick={handleConfirm} >Confirmar</Button>
                <Button onClick={() => history.push('/form')} >Alterar</Button>
            </div>
        </main>
    )
}

export default Confirm