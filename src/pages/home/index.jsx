import React from 'react'
import Button from '../../components/button'
import Swal from 'sweetalert2'
import { Link } from 'react-router-dom'
import './style.css'

const Home = () => {

    if (!!localStorage.getItem('message')) {
        Swal.fire({ title: localStorage.message })
        localStorage.removeItem('message')
    }

    return (
        <Link to='/form'>
            <Button>Cadastro</Button>
        </Link>
    )
}

export default Home