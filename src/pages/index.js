import Home from './home'
import Aside from './aside'
import Confirm from './confirm'
import Form from './form'

export { Home, Aside, Confirm, Form }