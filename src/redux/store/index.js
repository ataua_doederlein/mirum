import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import userData from '../reducers';

const store = createStore(userData, applyMiddleware(thunk));

export default store;
