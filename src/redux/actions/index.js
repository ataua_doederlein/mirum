export const UPDATE_DATA = 'update data';
export const UPDATE_IMG = 'update image';

export const updateData = (data) => ({
  type: UPDATE_DATA,
  data
});

export const updateImage = (img) => ({
  type: UPDATE_IMG,
  img
});