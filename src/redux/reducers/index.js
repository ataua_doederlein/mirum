import { UPDATE_DATA, UPDATE_IMG } from '../actions';

export const defaultState = {
  data: {
    first: '',
    last: '',
    age: 0,
    email: '',
    phone: '',
    state: '',
    country: '',
    home: '',
    work: '',
    interests: [],
    newsletter: false,
  },
  img: undefined,
}

const userData = (state = defaultState, action) => {
  const { type } = action;
  switch (type) {

    case UPDATE_DATA:
      console.log(action.data)
      return {
        data: action.data,
        img: state.img,
      };

    case UPDATE_IMG:
      return {
        data: state.data,
        img: action.img,
      };

    default:
      return state;
  }
};

export default userData;
