## Front End Jr

Foi-me apresentado um layout para replicar em React seguindo algumas diretrizes.
O resultado obtido tem algumas ligeiras diferenças do layout proposto, que eu aqui justifico:

- Foi utilizada uma imagem como placeholder pois esta é a parte que eu menos domino. Pesquisei um pouco e pensei em fazer alguma lógica em PHP ou Flask, mas essas são tecnologias de back end, que não correspondem ao propósito da vaga, e meu conhecimento nelas é rudimentar, então caso eu conseguisse algum resultado com elas, eu talves estivesse demonstrando uma habilidade que no fundo eu (ainda) não tenho. Considerei mais coerente usar um placeholder, justamente para identificar quais são os limites do meu conhecimento atual;

- Da mesma forma, não tinha um endpoint para onde apontar o post da submissão do formulário; criei um fake server com `my json server`, que eu achei por acaso e evitou que eu tivesse que configurar um `json server` ou um servidor em Flask só para isso. Tudo o que o `my json server` faz é retornar um id sequencial para o caso de a requisição estar correta.

- O input `type range` merecia mais consideração da minha parte no que tange a estilização, mas tendo que lidar com outros conflitos, deixei este ponto para trás. De maneira geral, no quesito estilização, tentei chegar próximo do que foi solicitado mas não atentei muito aos detalhes pois o prazo era curto; era solicitada habilidade em CSS; alternei o uso de CSS e styled components, que eu acho bem dinâmico, para dar uma noção de quanto eu domino cada recurso.

- Não proponho que eu conheça todos os recursos aqui apresentados de cor; muita coisa é fruto de pesquisa, algumas ideias vieras de videos do Youtube, algumas eu conhecia superficialmente e aprofundei para este projeto, como o YUP por exemplo. Mas, além de usar este teste para me aprofundar nos conhecimentos exigidos, creio que o trabalho em si de um desenvolvedor envolve pesquisar e testar continuamente novas soluções, e isso é justamente o que me atrai nessa área.

- Espero que minha tentativa atinja o padrão de qualidade pretendido, e em qualquer caso, apreciaria que um feedback fosse apresentado, apontando os pontos de melhora possíveis.

Atenciosamente,

### Atauã.

contato@ataua.com
